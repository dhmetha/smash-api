import lowdb from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

export const COLLECTIONS = {
    CHARACTERS: 'characters',
    UNIVERSES: 'universes'
};

const adapter = new FileSync('db.json');
const db = lowdb(adapter);

db.defaults({
    [COLLECTIONS.CHARACTERS]: [],
    [COLLECTIONS.UNIVERSES]: []
}).write();

export default db;
