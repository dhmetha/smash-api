import db from './db';

export function getFromSearch(table, search) {
    let x;
    
    if (isNaN(parseInt(search))) x = db.get(table)
        .find({ name: search })
        .value();
    
    else x = db.get(table)
        .find({ id: parseInt(search) })
        .value();

    return (x) ? x : false;
};

export function getFromSearchAsync(table, search) {
    return new Promise((resolve, reject) => {
        resolve(getFromSearch(table, search));
    });
};