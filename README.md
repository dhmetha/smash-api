# **smash-api**

Le but de ce projet est de mettre à disposition une base de donées permettant de lister, créer et modifier des personngage de l'univers de Super Smash Bros : Ultimate

## Fait par
 * Thibaud HARDY
 * Xavier MARTIN-WALLERAND

# Installation

## Node 10.x

Ce projet Utilise NodeJS, il faut donc l'installer : [https://nodejs.org/en/](https://nodejs.org/en/)

Puis vérifier que NodeJs est bien installé avec : 

```bash
$ node --version
v10.x
```
Si le numéro de version d'affiche, l'installation est correctt

## Installation des dépendences

Le projet utilise des dépendance qu'il faut installer en faisant :

```bash
npm install
```
# Démarrer l'application

Pour démarrer l'application lancer :

```bash
npm start
```

# Tester l'application

Pour tester l'application lancer : 

```bash
npm run test
```

# Les Routes

## Personnages

| Method | URI                                     | Description                                                                              |
|-------:|-----------------------------------------|------------------------------------------------------------------------------------------|
|    GET | **/api/c/**                             | Retourne la liste de tous les personnages                                                |
|    GET | **/api/c/`search`**                     | Retourne le personnage ayant l'id ou le nom recherché par `search`                       |
|   POST | **/api/c/`universe`/`name`**            | Ajoute le personnage avec son `universe` et `name` demandé                               |
|    PUT | **/api/c/`search`/name/`new_name`**     | Change le nom du personnage ayant l'id ou le nom recherché par `search` en `new_name`    |
|    PUT | **/api/c/`search`/universe/`universe`** | Change l'univers du personnage ayant l'id ou le nom recherché par `search` en `universe` |
| DELETE | **/api/c/`search`**                     | Supprime le personnage ayant l'id ou le nom recherché par `search`                       |

## Univers

| Method | URI                                 | Description                                                                          |
|-------:|-------------------------------------|--------------------------------------------------------------------------------------|
|    GET | **/api/u/**                         | Retourne la liste de tout les univers                                                |
|    GET | **/api/u/`search`**                 | Retourne l'univers ayant l'id ou le nom recherché par `search`                       |
|   POST | **/api/u/`name`**                   | Ajoute l'univers au `name` demandé                                                   |
|    PUT | **/api/u/`search`/name/`new_name`** | Change le nom de l'univers ayant l'id ou le nom recherché par `search` en `new_name` |
| DELETE | **/api/u/`search`**                 | Supprime l'univers ayant l'id ou le nom recherché par `search`                       |