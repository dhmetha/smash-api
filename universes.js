import express from 'express';
import db, { COLLECTIONS } from './db';

const router = express.Router();

router.post('/:name', ({ params: name }, res) => {
    getFromSearchAsync(COLLECTIONS.UNIVERSES, name).then((u) => {
        if (u){
            res.status(409).send(`Universe already exist with name: ${name}`);
            return;
        }
    
        u = {
            id: (db.get(COLLECTIONS.UNIVERSES).value()).length + 1,
            name: name.name
        };
        
        db.get(COLLECTIONS.UNIVERSES)   
            .push(u)
            .write();
        
        res.status(204).send();
    });
});

router.delete('/:search', (req, res) => {
    getFromSearchAsync(COLLECTIONS.UNIVERSES, req.params.search).then((u) => {
        if (!u) {
            res.status(404).send();
            return;
        }
        
        db.get(COLLECTIONS.UNIVERSES)
            .remove({ id: u.id })
            .write();
        
        res.status(204).send();
    });
});

router.put('/:search/name/:name', (req, res) => {
    getFromSearchAsync(COLLECTIONS.UNIVERSES, req.params.search).then((u) => {
        if (!u) {
            res.status(404).send();
            return;
        }
    
        db.get(COLLECTIONS.UNIVERSES)
            .find({ name: u.name })
            .assign({name: req.params.name})
            .write();
    
        res.status(204).send();
    });
})

router.get('/:search', (req, res) => {
    getFromSearchAsync(COLLECTIONS.UNIVERSES, req.params.search).then((u) => {
        if (!u) {
            res.status(404).send();
            return;
        }
    
        res.send(u);
    });
});

router.get('/', (req, res) => {
    res.send(db.get(COLLECTIONS.UNIVERSES).value());
});

export default router;