import express from 'express';
import api from './api';

const app = express();
const port = 80;

app.use(express.static("public"));

app.use('/api', api);

app.listen(port, () => {
    console.log(`listening on *:${port}`)
});