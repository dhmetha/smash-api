import express from 'express';
import db, { COLLECTIONS } from './db';
import { getFromSearchAsync } from './utils';

const router = express.Router();

router.post('/:universe/:name', ({ params: { universe, name } }, res) => {
    getFromSearchAsync(COLLECTIONS.CHARACTERS, name).then((c) => {
        getFromSearchAsync(COLLECTIONS.UNIVERSES, universe).then((u) => {
            if (c){
                res.status(409).send(`Character already exist with name: ${name}`);
                return;
            }
        
            if (!u) {
                res.status(404).send(`Universe not found with id: ${universe}`);
                return;
            }
        
            let id, allc = db.get(COLLECTIONS.CHARACTERS).value();
        
            if (allc[allc.length-1]) id = (allc[allc.length-1].id + 1);
            else id = 1;
        
            c = {
                id,
                name,
                universe: u.id
            };
            db.get(COLLECTIONS.CHARACTERS)
                .push(c)
                .write();
            
            res.status(204).send();
        });
    });
});

router.get('/', (req, res) => {
    res.send(db.get(COLLECTIONS.CHARACTERS).value());
});

router.get('/:search', (req, res) => {
    getFromSearchAsync(COLLECTIONS.CHARACTERS, req.params.search).then((c) => {
        if (!c) {
            res.status(404).send();
            return;
        }
    
        res.send(c);
    });
});

router.put('/:search/name/:name', ({ params: { search, name } }, res) => {
    getFromSearchAsync(COLLECTIONS.CHARACTERS, search).then((c) => {
        if (!c) {
            res.status(404).send();
            return;
        }
    
        db.get(COLLECTIONS.CHARACTERS)
            .find({ id: c.id })
            .assign({ name })
            .write();
        
        res.status(204).send();
    });
});

router.put('/:search/universe/:universe', ({ params: { search, universe } }, res) => {
    getFromSearchAsync(COLLECTIONS.CHARACTERS, name).then((c) => {
        getFromSearchAsync(COLLECTIONS.UNIVERSES, universe).then((u) => {
            if (!c || !u) {
                res.status(404).send();
                return;
            }

            db.get(COLLECTIONS.CHARACTERS)
                .find({ id: c.id })
                .assign({ universe: u.id })
                .write();
            
            res.status(204).send();
        });
    });
});

router.delete('/:search', (req, res) => {
    getFromSearchAsync(COLLECTIONS.CHARACTERS, req.params.search).then((c) => {
        if (!c) {
            res.status(404).send();
            return;
        }
        
        db.get(COLLECTIONS.CHARACTERS)
            .remove({ id: c.id })
            .write();
        
        res.status(204).send();
    })
});

export default router;