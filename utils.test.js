//Import des fonctions
import { getFromSearch } from './utils'
import db, { COLLECTIONS } from './db';

// remise à 0 de la db
// -----------------------------------------------------------------------------
function reset(){
    db.set(COLLECTIONS.CHARACTERS, []).write()
    db.set(COLLECTIONS.UNIVERSES, []).write()
}
afterAll(reset)
describe('testDbConnection', ()=>{
    test('Valid connection', () => {
        expect(db.get(COLLECTIONS.CHARACTERS)).toBeDefined()
    })
    test('utils.insertUniverse', () =>{
    expect(db.get(COLLECTIONS.UNIVERSES)
        .push({
            id: 1,
            name: "Mario"
        }).write()
    ).toBeDefined()
    }),
    test('utils.insertCharacter', () =>{
        expect(db.get(COLLECTIONS.CHARACTERS)
        .push({
            id: 1,
            name: "Luigi",
            universe: 1
        }).write()
    ).toBeDefined()
    })
}),
describe('utils.getFromSearch', ()=>{
    test('utils.getOneResult', ()=>{
        expect(getFromSearch(COLLECTIONS.CHARACTERS, 1)).toEqual({ id: 1, name: "Luigi", universe: 1})
    })
});