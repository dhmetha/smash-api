import express from 'express';
import characters from './characters';
import universes from './universes';

const router = express.Router();

router.use('/c', characters);
router.use('/u', universes);

export default router;